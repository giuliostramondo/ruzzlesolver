//
//  main.c
//  Ruzzle
//
//  Created by Giulio Stramondo on 23/12/12.
//  Copyright (c) 2012 Giulio Stramondo. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "graph.h"
#include "word.h"
#include "dictionary.h"







//This function create a string containing the visitedNodes id separated by spaces
char* movesToString(NodeList* visitedNodes){
    int i=0;
    NodeList *tmp;
    tmp=visitedNodes;
    for(i=0;tmp!=NULL;i++){
        tmp=tmp->next;
    }
    char* res;
    char* resfin;
    char* str;
    res=(char*) malloc(sizeof(char)*30);
    resfin=(char*) malloc(sizeof(char)*30);
    tmp=visitedNodes;
    for(i=0;tmp!=NULL;i++){
        str=(char*)malloc(sizeof(char)*4);
        sprintf(str, "%d ", tmp->elem->id);
        if(i==0){
            strcpy (res,str);
        }else{
            strcat (res,str);
        }
        tmp=tmp->next;
    }
    sprintf(resfin," %s",res);
    return resfin;
}

//TODO Implementare questa funzione che stampa la griglia con i links
//TODO per far ciò serve una funzione che ricostruisca la lista dei nodi da una stringa o direttamente produca una arc list di archi corrispondenti alle mosse
void printMoveMap(Graph* g,char* movements,int offset){
    char links[33];
    char tmpSubStr[5][10];
    int i,j;
    NodeList* nodes;
    nodes=g->nodes;
    
    
    char * space;
    space=malloc(sizeof(char)*20);
    for(int i=0;i<offset;i++){
        if(i==0)strcpy (space," ");
        else strcat (space," ");
    }
    //char* movements=" 14 9 8 5 1 2 7 10 6 ";
    
    for(i=0;i<=2;i++){
        for(j=0;j<=3;j++){
            if(i!=2){
                //printf("links[%d] dipende da %d e %d\n",(j*10)+i,((((j*10)+i)/2)-j+i),((((j*10)+i)/2)-j+i)+1);
                sprintf(tmpSubStr[0], " %d %d ",((((j*10)+i)/2)-j+i),((((j*10)+i)/2)-j+i)+1);
                sprintf(tmpSubStr[1], " %d %d ",((((j*10)+i)/2)-j+i)+1,((((j*10)+i)/2)-j+i));
                if((strstr(movements,tmpSubStr[0])!=NULL)||(strstr(movements,tmpSubStr[1])!=NULL)){
                  //  printf("-\n");
                    links[(j*10)+i]='-';
                }
                else{
                    //printf("nope\n");
                    links[(j*10)+i]=' ';
                }
                
            }
            else{
                //printf("links[%d] dipende da %d e %d\n",(j*10)+i,((((j*10)+i)/2)-j+i-1),((((j*10)+i)/2)-j+i));
                sprintf(tmpSubStr[0], " %d %d ",((((j*10)+i)/2)-j+i-1),((((j*10)+i)/2)-j+i));
                sprintf(tmpSubStr[1], " %d %d ",((((j*10)+i)/2)-j+i),((((j*10)+i)/2)-j+i)-1);
                if((strstr(movements,tmpSubStr[0])!=NULL)||(strstr(movements,tmpSubStr[1])!=NULL)){
                   // printf("-\n");
                    links[(j*10)+i]='-';
                }
                else{
                  //  printf("nope\n");
                    links[(j*10)+i]=' ';
                }
            }

        }
    }
    
    
    for(i=0;i<=3;i++){
        for(j=0;j<=2;j++){
            
               // printf("links[%d] dipende da %d e %d\n",(j*10)+3+2*i,(((3+10*j)/2)-(j+1)+i),(((3+10*j)/2)-(j+1)+i)+4);
                sprintf(tmpSubStr[0], " %d %d ",(((3+10*j)/2)-(j+1)+i),(((3+10*j)/2)-(j+1)+i)+4);
                sprintf(tmpSubStr[1], " %d %d ",(((3+10*j)/2)-(j+1)+i)+4,(((3+10*j)/2)-(j+1)+i));
                if((strstr(movements,tmpSubStr[0])!=NULL)||(strstr(movements,tmpSubStr[1])!=NULL)){
               //     printf("|\n");
                    links[(j*10)+3+2*i]='|';
                }
                else{
                  //  printf("nope\n");
                    links[(j*10)+3+2*i]=' ';
                }
            
        }
    }
    
    for(i=0;i<=2;i++){
        for(j=0;j<=2;j++){

             //   printf("links[%d] dipende da %d e %d, %d e %d\n",4+(2*j)+(i*10),j+(4*i),j+(4*i)+5,j+(4*i)+4,j+(4*i)+1);
                sprintf(tmpSubStr[0], " %d %d ",j+(4*i),j+(4*i)+5);
                sprintf(tmpSubStr[1], " %d %d ",j+(4*i)+5,j+(4*i));
                sprintf(tmpSubStr[2], " %d %d ",j+(4*i)+4,j+(4*i)+1);
                sprintf(tmpSubStr[3], " %d %d ",j+(4*i)+1,j+(4*i)+4);
           // printf("tmpSubStr[0]:%s tmpSubStr[1]:%s tmpSubStr[2]:%s tmpSubStr[3]:%s\n",tmpSubStr[0],tmpSubStr[1],tmpSubStr[2],tmpSubStr[3]);
            if(((strstr(movements,tmpSubStr[0])!=NULL)||(strstr(movements,tmpSubStr[1])!=NULL))&&((strstr(movements,tmpSubStr[2])!=NULL)||(strstr(movements,tmpSubStr[3])!=NULL))){
               //     printf("X\n");
                    links[4+(2*j)+(i*10)]='X';
            }
                else {
                    if((strstr(movements,tmpSubStr[0])!=NULL)||(strstr(movements,tmpSubStr[1])!=NULL)){
                       // printf("\\\n");
                        links[4+(2*j)+(i*10)]='\\';
                    }
                    else{
                    
                    if((strstr(movements,tmpSubStr[2])!=NULL)||(strstr(movements,tmpSubStr[3])!=NULL)){
                       // printf("/\n");
                        links[4+(2*j)+(i*10)]='/';
                        }
                    else{
                      //  printf("nope\n");
                        links[4+(2*j)+(i*10)]=' ';
                    }
                    }
                }

        }
    }
    
    //Printing MAP
    printf("%s",space);
    for(int i=0;i<3;i++){

    printf("%c%c",nodes->elem->letter,links[i]);
            nodes=nodes->next;    
    }
    printf("%c",nodes->elem->letter);nodes=nodes->next;
    
    printf("\n%s",space);
    for(i=3;i<10;i++){
        printf("%c",links[i]);
    }
    printf("\n%s",space);
    for(int i=10;i<13;i++){
        printf("%c%c",nodes->elem->letter,links[i]); nodes=nodes->next;
    }
    printf("%c",nodes->elem->letter);nodes=nodes->next;
    printf("\n%s",space);
    for(i=13;i<20;i++){
        printf("%c",links[i]);
    }
    printf("\n%s",space);
    for(int i=20;i<23;i++){
        printf("%c%c",nodes->elem->letter,links[i]); nodes=nodes->next;
    }
    printf("%c",nodes->elem->letter);nodes=nodes->next;
        printf("\n%s",space);
    for(i=23;i<30;i++){
        printf("%c",links[i]);
    }
    printf("\n%s",space);
    for(int i=30;i<33;i++){
        printf("%c%c",nodes->elem->letter,links[i]); nodes=nodes->next;
    }
    printf("%c",nodes->elem->letter);
    printf("\n");
}

void printListAndMap(WordList *list, Graph* g){
    WordList *tmp;
    tmp=list;
    printf("\n");
    while(tmp!=NULL){
        printf("%s \n",tmp->elem->string);

        printMoveMap(g,tmp->elem->moves, 20);
        printf("\n\n");
        tmp=tmp->next;
    }
}
//This function finds all the word starting at NodeId of lenght wordLenght that are in the dictionary diz
//Returns: a WordList containing the words found
//ARGUMENTS
//g: is the graph containing the Ruzzle matrix
//NodeId: is the starting node ( and its also used in the recursion )
//visitedNodes: its a NodeList used to not use two times the same letter during the recursion and to memorize the moves made to create the word (Initially is set to NULL)
//current: memorize the word currently builded (initially is set to NULL), at the end of the recursion this word will be checked and if present in diz will be put in list ( the returned WordList )
//list: is the returned value (initially set to NULL ) used in the recursion
//diz: is the dictionary used to look up the words, it has to be already populated
WordList* getWordsFromNode(Graph *g,int NodeId, NodeList* visitedNodes, int wordLenght, int depth, char *current,WordList* list,char** diz,int dizSize){
    NodeList* tmp;
    NodeList* NodeListTmp1;
    NodeList* forwardStar;

    //Take the pointer to the Node with id NodeId from the graph g
    tmp=fetchNode(g->nodes, NodeId);
    
    //Allocates the memory for the variable current at the beginning of recursion ( it's a string of 30 char )
    if(depth==0){
        current= (char*) malloc(sizeof(char)*30);
    }
    
    //Puts the current node (tmp) into the list of visited nodes
    visitedNodes= AddNodeToList(visitedNodes, tmp->elem);
    
    //Write the letter contained into the current node into the string current at position depth
    current[depth]=tmp->elem->letter;
    
    //Checks if the maximum wordLenght is been reached, if so finalize the string current ('\0') look for it in the dictionary, if found puts current into list
    if(depth==wordLenght-1){


        current[depth+1]='\0';
        
        if(searchInDictionary(diz, dizSize, current)!=-1){
        if(list==NULL){
            list=createWordList(createWord(current,movesToString( visitedNodes)));
        }else{
            list=addWordToList(list, createWord(current,movesToString( visitedNodes)));
        }
        }
       /* 

        printf("WordList: ");PrintWordList(list);*/
        return list;
        
    }
    
    //Looks for all the nodes linked to tmp (from tmp, to * ) and puts it into a NodeList
    forwardStar=ForwardStar(g, tmp->elem->id);
    
    //For each node in the forwardStar if it hasn't been visited yet, launch the recursion
    while(forwardStar!=NULL){

        if(!listContainsNode(visitedNodes, forwardStar->elem)){
        

            list=getWordsFromNode(g,forwardStar->elem->id, visitedNodes, wordLenght, depth+1, current,list,diz,dizSize);
            NodeListTmp1=visitedNodes;
            
            //Remove the last node because it and its children have already been explored
            while(NodeListTmp1->next!=NULL)NodeListTmp1=NodeListTmp1->next;
            NodeListTmp1->previous->next=NULL;
            free(NodeListTmp1);
            
                //Creates a new variable current ( because the old one has been filled with the childrens )
                current= (char*) malloc(sizeof(char)*30);

            
                //Populates the new current variables with the visitedNodes before the current node (current node included )
                NodeListTmp1=visitedNodes;
                for(int i=0;NodeListTmp1!=NULL;i++){
                current[i]=NodeListTmp1->elem->letter;
                NodeListTmp1=NodeListTmp1->next;

            }
        }
        forwardStar = forwardStar->next;
    }
    
    
    
    return list;
}


//Function called by main which calls the recursive function getWordFromNode for each node and with maxLenght from 2 to maxLength passed as arg
//Returns a WordList with all word found ( already checked in the Dictionary passed (diz)
WordList* getWords(Graph* g,WordList* list,int maxLenght,char** diz,int dizSize){
    NodeList *n;
    n=g->nodes;
    
    //Creates a progress bar, just to have an idea of how long its going to take, if maxLenght is more than 20 go get a coffie...
    char * progressbar=malloc(sizeof(char)*18);
    progressbar[0]='[';
    for(int i=0;i<16;i++)progressbar[i+1]=' ';
    progressbar[17]=']';
    
    //Calls the recursive function where i is the node, k is the maxLenght of the words, updates the progressbar and prints it
    for(int i=0;i<16;i++){
        for(int k=2;k<=maxLenght;k++)
            list=getWordsFromNode(g, i, NULL, k, 0, NULL, list,diz,dizSize);
        progressbar[i+1]='*';
        printf("%s\n",progressbar);
    }
    return list;
}



int main(int argc, const char * argv[])
{

    char array[4][4];
    //char diz[60454][30];
    char **dict;
    //FILE *dizionario;
    int dictSize;
    

    //dizionario=fopen("/Users/giuliostramondo/Projects/Università/Ricerca Operativa/Ruzzle/Ruzzle/Ruzzle/words_italian.win.txt","r");
    
    //if (dizionario==NULL) { /* error opening file returns NULL */
     //   printf("Could not open data.txt!\n"); /* error message */
      //  return 1; /* exit with failure */
    //}

    //Loads the dictionary into the array char diz
    
    
    //for(int i=0;i<60454;i++)
     //   getLine(dizionario, diz[i]);
    
    
    
    //Prints the dictionary
    /*for(int i=0;i<60454;i++)
        printf("%s\n",diz[i]);*/ 
    
    //For testing purpose uncomment the argc=2 and argv[1]="blablabla..." and put in argv[1] the ruzzle martix to solve (all in the same line)
   /// argc=2;
    if(argc<2){
        printf("\nDevi inserire una stringa contenente le lettere della matrice ( in fila ) [e la path al dizionario da usare]\n");
        return 0;
    }
    
    if(argc<3)
    loadDictionary("/Users/giuliostramondo/Projects/Università/Ricerca Operativa/Ruzzle/Ruzzle/Ruzzle/words_italian.win.txt", &dictSize, &dict);
    else
    loadDictionary(argv[2], &dictSize, &dict);
    if(dict==NULL)printf("didnt work");
   
    int k=0;
    
   /// argv[1]="ireclgirimacauep";
  
    for(int i=0;i<4;i++)
        for(int j=0;j<4;j++){
            array[i][j]=argv[1][k];
            k++;
        }
      printf("\n");
    
    for(int i=0;i<4;i++){
        for(int j=0;j<4;j++){
            printf("%c",array[i][j]);
        }
        printf("\n");
    }
    
    //Create the graph
    NodeList *try1=createNodes(16);
    Graph *g=malloc(sizeof(Graph));
    g->nodes=try1;
    
    
    //Link the graph node as the ruzzle board is linked
    g->nodes->elem->arcs=NULL; //PERCHE' Cazzo ci vuole questo!?
    addArcBidirectional(g, 0, 1, 1);
    addArcBidirectional(g, 0, 5, 1);
    addArcBidirectional(g, 0, 4, 1);
    addArcBidirectional(g, 1, 4, 1);
    addArcBidirectional(g, 1, 5, 1);
    addArcBidirectional(g, 1, 6, 1);
    addArcBidirectional(g, 1, 2, 1);
    addArcBidirectional(g, 2, 5, 1);
    addArcBidirectional(g, 2, 6, 1);
    addArcBidirectional(g, 2, 7, 1);
    addArcBidirectional(g, 2, 3, 1);
    addArcBidirectional(g, 3, 6, 1);
    addArcBidirectional(g, 3, 7, 1);
    addArcBidirectional(g, 4, 8, 1);
    addArcBidirectional(g, 4, 9, 1);
    addArcBidirectional(g, 4, 5, 1);
    addArcBidirectional(g, 5, 8, 1);
    addArcBidirectional(g, 5, 9, 1);
    addArcBidirectional(g, 5, 10, 1);
    addArcBidirectional(g, 5, 6, 1);
    addArcBidirectional(g, 6, 9, 1);
    addArcBidirectional(g, 6, 10, 1);
    addArcBidirectional(g, 6, 11, 1);
    addArcBidirectional(g, 6, 7, 1);
    addArcBidirectional(g, 7, 10, 1);
    addArcBidirectional(g, 7, 11, 1);
    addArcBidirectional(g, 8, 9, 1);
    addArcBidirectional(g, 8, 12, 1);
    addArcBidirectional(g, 8, 13, 1);
    addArcBidirectional(g, 9, 12, 1);
    addArcBidirectional(g, 9, 13, 1);
    addArcBidirectional(g, 9, 14, 1);
    addArcBidirectional(g, 9, 10, 1);
    addArcBidirectional(g, 10, 13, 1);
    addArcBidirectional(g, 10, 14, 1);
    addArcBidirectional(g, 10, 15, 1);
    addArcBidirectional(g, 10, 11, 1);
    addArcBidirectional(g, 11, 14, 1);
    addArcBidirectional(g, 11, 15, 1);
    addArcBidirectional(g, 12, 13, 1);
    addArcBidirectional(g, 13, 14, 1);
    addArcBidirectional(g, 14, 15, 1);
    
    NodeList *tmp;
    
    //Writes the letters in the respective graph nodes
    for (int k=0; k<16; k++) {
        tmp=fetchNode(g->nodes, k);
        tmp->elem->letter=argv[1][k];
    }
    
    
        //Max lenght for the word to look for ( used to stop the recursive algorithm )
        int lenght=12;
    
    //printGraphStructure(g);
     
    WordList * list = NULL;
    
    //Does the heavy code :P
    list = getWords(g, list, lenght,dict,dictSize);


    //printf("WordList:             Moves: ");PrintWordList(list);
    
    
    list=OrderWordsFromSmallerToBigger(list);
  //  printf("\n\nWordList:             Moves: ");PrintWordList(list);
    printf("\n\nWordList:           Moves: ");    
    printListAndMap(list,g);
    
    
   /* if(searchInDictionary(diz, 60454, "scosso")!=-1)printf("trovato");
    else printf("non trovato");*/
    
    
    
    /*
      ->
      j=0123
    k=0 ruzz
    | 1 leru
    v 2 zzle
      3 ruzz
     
     
     0  1  2  3
     4  5  6  7
     8  9 10 11
    12 13 14 15
     
     
     this example 
     irec
     lgir
     imac
     auep
     */
    
    
    
}


