//
//  dictionary.h
//  Ruzzle
//
//  Created by Giulio Stramondo on 04/01/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifndef Ruzzle_dictionary_h
#define Ruzzle_dictionary_h

int countLines(FILE* fptr){
    
    if(fptr==0)return -1;
    int count = 0;
    int status = 0;
    char temp[50];


    while (status != EOF)
    {
        status = fscanf(fptr, "%s\n", temp);
        count++;
    }
    
    return count;

}

//Gets a line from filepointer and puts it into string
void getLine(FILE *filepointer,char *string){
    char tmp;
    int i=0;
    tmp=fgetc(filepointer);
    while(tmp!='\0'&&tmp!='\n'&&tmp!='\r'&&tmp!='\xff'){
        string[i]=tmp;
        tmp=fgetc(filepointer);
        i++;
    }
    string[i]='\0';
}

void loadDictionary(char* path,int* size,char*** dictionary){
    FILE *dizionario;
    dizionario=fopen(path,"r");
    int dim;
    if (dizionario==NULL) { /* error opening file returns NULL */
        printf("Could not open data.txt!\n"); /* error message */
        *dictionary=NULL; /* exit with failure */
    }
    
    dim=countLines(dizionario);
    fclose(dizionario);
    dizionario=fopen(path,"r");
    
    *dictionary= (char**)malloc(sizeof(char*)*(dim));
    
    for(int i=0;i<dim;i++){
        (*dictionary)[i]=(char*)malloc(sizeof(char)*30);
        getLine(dizionario,(*dictionary)[i]);
    }
    *size=dim;

}



//Binary search algorithm used to look for a word in the dictionary ( the dictionary's words have to be ordered from a-z )
int searchInDictionary(char** dictionary,int size,char* word){
    int start = 0, end = size - 1, centro = 0;
    while (start <= end)
    {
        centro = (start + end) / 2;
        if (strcmp(dictionary[centro],word)>0)
        {
            end = centro - 1;
        }
        else
        {
            if (strcmp(dictionary[centro],word)<0)
                start = centro + 1;
            else
                return centro; // Caso: elemento==array[centro]
        }
    }
    return -1;
}
#endif
