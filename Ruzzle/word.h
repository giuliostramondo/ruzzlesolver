//
//  word.h
//  Ruzzle
//
//  Created by Giulio Stramondo on 28/12/12.
//  Copyright (c) 2012 Giulio Stramondo. All rights reserved.
//

#ifndef Ruzzle_word_h
#define Ruzzle_word_h

typedef struct word{
    char* string;
    char* moves;
}Word;

typedef struct wordlist{
    Word *elem;
    struct wordlist *next;
    struct wordlist *prev;
}WordList;

void switchWordListElem(WordList* word1,WordList* word2){
    WordList* tmp;
    tmp=(WordList*)malloc(sizeof(WordList));
    tmp->elem=word1->elem;
    word1->elem=word2->elem;
    word2->elem=tmp->elem;
    free(tmp);
}

int getWordSize(Word* word){
    int i=0;
    for(i=0;word->string[i]!='\0';i++);
    return i;
}

Word* createWord(char* origin,char* moves){
    int i=0;
    for(i=0;origin[i]!='\0';i++);
    Word* dest;
    char* str;
    dest = (Word*) malloc(sizeof(Word));
    str= (char*) malloc(sizeof(char)*i);
    dest->string=str;
    for(int j=0;j<i;j++)
        dest->string[j]=origin[j];
    free(origin);
    dest->moves=moves;
    return dest;
}

WordList* createWordList(Word* first){
    WordList* list;
    list=(WordList*)malloc(sizeof(WordList));
    list->next=NULL;
    list->prev=NULL;
    list->elem=first;
    return list;
}

WordList* addWordToList(WordList* list,Word* wor){
    WordList* tmp;
    WordList* old;
    tmp=list;
    while(tmp!=NULL){old=tmp;tmp=tmp->next;}
    WordList* newWord;
    newWord=malloc(sizeof(WordList));
    old->next=newWord;
    newWord->elem=wor;
    newWord->prev=old;
    newWord->next=NULL;
    return list;    
}


void PrintWordList (WordList* list){
    WordList *tmp;
    tmp=list;
    printf("\n");
    while(tmp!=NULL){
        printf("%s                  %s\n",tmp->elem->string,tmp->elem->moves);
        tmp=tmp->next;
    }
}

WordList* OrderWordsFromBiggerToSmaller(WordList* list){
    WordList* tmp;
    WordList* tmp1;
    tmp=list;
    while(tmp->next!=NULL){
        tmp1=tmp->next;
        while(tmp1!=NULL){
            if(getWordSize(tmp1->elem)>getWordSize(tmp->elem)){
                switchWordListElem(tmp1, tmp);
            }
            tmp1=tmp1->next;
        }
        
        tmp=tmp->next;
    }
    return list;
}
WordList* OrderWordsFromSmallerToBigger(WordList* list){
    WordList* tmp;
    WordList* tmp1;
    tmp=list;
    while(tmp->next!=NULL){
        tmp1=tmp->next;
        while(tmp1!=NULL){
            if(getWordSize(tmp1->elem)<getWordSize(tmp->elem)){
                switchWordListElem(tmp1, tmp);
            }
            tmp1=tmp1->next;
        }
        
        tmp=tmp->next;
    }
    return list;
}
#endif
