//
//  graph.h
//  Kruskal algorithm
//
//  Created by Giulio Stramondo on 25/11/12.
//  Copyright (c) 2012 Giulio Stramondo. All rights reserved.
//

#ifndef Kruskal_algorithm_graph_h
#define Kruskal_algorithm_graph_h

typedef struct node{
    int id;
    char letter;
    struct arclist *arcs;
}Node;


typedef struct arc{
    Node *from;
    Node *to;
    int weight;
}Arc;

typedef struct nodelist{
    Node *elem;
    struct nodelist *next;
    struct nodelist *previous;
}NodeList;


typedef struct arclist{
    Arc *elem;
    struct arclist *next;
    struct arclist *previous;
}ArcList;


typedef struct graph{
    NodeList *nodes;
    ArcList *arcs;
}Graph;

ArcList* fetchArc(ArcList* list,Arc* arc){
    ArcList *tmp;
    tmp=list;
    while(tmp!=NULL){
        if((tmp->elem->to==arc->to)&&(tmp->elem->from==arc->from))
            return tmp;
    }
    return NULL;
}

ArcList* addArcToArcList(ArcList* list, Arc* a){
    if(list==NULL){
        list=(ArcList*)malloc(sizeof(ArcList));
        list->elem=a;
        return list;
    }
    ArcList* tmp;
    ArcList* newEl;
    newEl=(ArcList*)malloc(sizeof(ArcList));
    tmp=list;
    while(tmp->next!=NULL){
        tmp=tmp->next;
    }
tmp->next=newEl;
newEl->previous=tmp;
newEl->next=NULL;
return list;
}

NodeList* createNodes(int number){
    if (number <=0) return NULL;
    NodeList *res;
    Node *element;
    NodeList *tmpnext;
    NodeList *tmpprev;
    NodeList *tmpcurrent;
    
    res = malloc(sizeof(NodeList));
    element = malloc(sizeof(Node));
    res->elem=element;
    res->elem->id=0;
    res->previous=NULL;
    tmpcurrent=res;
    tmpnext=malloc(sizeof(NodeList));
    element = malloc(sizeof(Node));
    tmpnext->elem=element;
    res->next=tmpnext;
    
    for (int i=1; i<number;i++){
        tmpprev=tmpcurrent;
        tmpcurrent=tmpnext;
        tmpcurrent->elem->id=i;
        tmpcurrent->previous=tmpprev;
        
        if(i!=number){
            tmpnext=malloc(sizeof(NodeList));
            element = malloc(sizeof(Node));
            tmpnext->elem=element;
        }
        else
            tmpnext=NULL;
        
        tmpcurrent->next=tmpnext;
    }
    return res;
}

NodeList *fetchNode(NodeList *list,int iD){
    if(list == NULL)return NULL;
    while(list!=NULL&&list->elem->id!=iD)list=list->next;
    return list;
}

void addArc(Graph *g,int from,int to,int weight){
    if(g->nodes == NULL)printf("\nGraph is empty\n");
    
    //Add arc to graph arclist ( create it if its NULL )
    
    NodeList *fromNode;
    NodeList *toNode;
    fromNode=fetchNode(g->nodes,from);
    toNode=fetchNode(g->nodes,to);
    ArcList * tmparclist;
    Arc *tmparc;
    
    if(g->arcs == NULL ){
        tmparclist = malloc(sizeof(ArcList));
        tmparc = malloc(sizeof(Arc));
        tmparclist->previous=NULL;
        tmparclist->next=NULL;
        tmparclist->elem=tmparc;
        g->arcs=tmparclist;
    }
    else{
        tmparclist=g->arcs;
        while(tmparclist->next!=NULL)tmparclist=tmparclist->next;
        ArcList *old;
        old=tmparclist;
        tmparclist = malloc(sizeof(ArcList));
        tmparc = malloc(sizeof(Arc));
        tmparclist->elem=tmparc;
        old->next=tmparclist;
        tmparclist->previous=old;
        tmparclist->next=NULL;
    }
    tmparclist->elem->from=fromNode->elem;
    tmparclist->elem->to=toNode->elem;
    tmparclist->elem->weight=weight;
    
    //Add arc to node fromnode arclist(creates it if its NULL)
    
    ArcList * tmparclist1;
    if(fromNode->elem->arcs==NULL){
        tmparclist1 = malloc(sizeof(ArcList));
        tmparclist1->elem=tmparc;
        tmparclist1->previous=NULL;
        tmparclist1->next=NULL;
        fromNode->elem->arcs=tmparclist1;
    }
    else{
        ArcList *old;
        tmparclist1=fromNode->elem->arcs;
        while(tmparclist1->next!=NULL)tmparclist1=tmparclist1->next;
        old=tmparclist1;
        tmparclist1=malloc(sizeof(ArcList));
        tmparclist1->elem=tmparc;
        old->next=tmparclist1;
        tmparclist1->previous=old;
    }
    tmparclist1->elem->from=fromNode->elem;
    tmparclist1->elem->to=toNode->elem;
    tmparclist1->elem->weight=weight;
    
}

void addArcBidirectional(Graph *g,int from,int to,int weight){
    addArc(g, from, to, weight);
    addArc(g, to, from, weight);
}

void printArcList(ArcList *listToPrint){
    if(listToPrint==NULL){printf("ArcList is empty\n");}
    else{
        ArcList *tmp=listToPrint;
        while(tmp!=NULL){
            printf(" (%d,%d) w=%d ",tmp->elem->from->id,tmp->elem->to->id,tmp->elem->weight);
            tmp=tmp->next;
        }
        printf("\n");
    }
}



void printNodeList(NodeList *listToPrint){
    if(listToPrint==NULL)printf("\nNodeList is empty\n");
    else{
        NodeList *tmp=listToPrint;
        while(tmp!=NULL){
            printf("\nNode id:%d",tmp->elem->id);
            tmp=tmp->next;
        }
        printf("\n");
    }
}


void printGraphStructure(Graph *g){
    if(g==NULL){printf("\nGraph is empty\n"); return;}
    NodeList *tmp=g->nodes;
    while(tmp->next!=NULL){
        printf("\nNode id:%d letter:%c\n",tmp->elem->id,tmp->elem->letter);
        printArcList(tmp->elem->arcs);
        tmp=tmp->next;
    }
    
}

int listContainsNode(NodeList *list, Node* node){
    while(list!=NULL){
        if(list->elem==node)return 1;
        list=list->next;
    }
    return 0;
}

NodeList* AddNodeToList(NodeList* list, Node* node){
    if(list==NULL){
        list=malloc(sizeof(NodeList));
        list->previous=NULL;
        list->next=NULL;
        list->elem=node;
    }
    else{
        NodeList *tmp;
        NodeList *newNode;
        newNode = malloc(sizeof(NodeList));
        newNode->next=NULL;
        newNode->elem=node;
        tmp=list;
        while(tmp->next!=NULL){
            tmp=tmp->next;
        }
        tmp->next=newNode;
        newNode->previous=tmp;
    }
    return list;
}

NodeList *ForwardStar(Graph *g, int node){
    NodeList *n=fetchNode(g->nodes, node);
    NodeList *res;
    //Se non ci sono archi uscenti dal nodo restituisci null
    if(n->elem->arcs==NULL){res=NULL;return res;}
    
    //Altrimenti alloca lo spazio per un elemento della lista
    
    res=malloc(sizeof(NodeList));
    res->previous=NULL;
    res->elem=n->elem->arcs->elem->to;
    
    NodeList *tmp;
    NodeList *old;
    
    res->next=NULL;
    old=res;
    ArcList* tmparc;
    tmparc=n->elem->arcs->next;
    while(tmparc!=NULL){
        tmp=malloc(sizeof(NodeList));
        tmp->elem=tmparc->elem->to;
        tmp->previous=old;
        old->next=tmp;
        tmp->next=NULL;
        old=tmp;
        tmparc=tmparc->next;
    }
    return res;
}



#endif
